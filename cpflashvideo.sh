#!/bin/bash
# Save the flash video written by the Flash 10.x plugin.

# Author: Paolo VIOTTI <paolo.viotti@gmail.com>

# License:
# Copyright 2011 Paolo Viotti
# This script is licensed under the GNU General Public License v3.0.
# You can obtain a copy of this license here: http://www.gnu.org/licenses/gpl.html

# Usage: to insert in the bashrc file and use as cpflashvideo /path/to/file.flv

function cpflashvideo {
    PID=$(lsof -c plugin-co | grep Flash | tail -n 1 | tr -s " " | cut -d" " -f2)
    B=$(lsof -c plugin-co | grep Flash | tail -n 1 | tr -s " " | cut -d" " -f4 | tr -d "[a-zA-Z]")
    cp /proc/$PID/fd/$B $1
}

