#!/bin/bash
# Transform an image-like pdf into a searchable OCR'd pdf

# Author: Paolo VIOTTI <paolo.viotti@gmail.com>

# License:
# Copyright 2011 Paolo Viotti
# This script is licensed under the GNU General Public License v3.0.
# You can obtain a copy of this license here: http://www.gnu.org/licenses/gpl.html

# Usage: pdfocf.sh <input.pdf>
# The output file will be called <input>_ocrd.pdf

# Requirements:
# pdftk, cuneiform (with corresponding language package), pdftoppm, hocr2pdf
# See also: pdfsandwhich, tesseract

if [ $# -ne 1 ]
then
  echo "Usage: `basename $0` {input.pdf}"
  exit 1
fi

mkdir tmp

# Get number of pages
num_pag=$(pdftk $1 dump_data | grep NumberOfPages | cut -d":" -f2 | cut -d" " -f2)

for((n=1; n<=$num_pag; n++))
do
    # Get a single page
    pdftk $1 cat $n output tmp/$n.pdf

    # Transform it into a PPM file
    pdftoppm -f 1 -l 1 -r 200 tmp/$n.pdf tmp/$n #-mono -r 300
    #convert -depth 8 tmp/$n.pdf tmp/$n.ppm

    # OCR
    cuneiform -l ita -f hocr -o tmp/$n.hocr tmp/$n-000001.ppm # --dotmatrix
    # using tesseract (with hocr support, from svn):
    #convert -depth 8 $n.pdf $n.tif
    #tesseract tar.tif output -l ita nobatch hocr

    # Create OCR'd pdf file
    hocr2pdf -i tmp/$n-000001.ppm -s -o tmp/$n-ocrd.pdf < tmp/$n.hocr
done

# Join the pages into a single pdf file
pdftk tmp/*-ocrd.pdf cat output $(basename $1 .pdf)_ocrd.pdf

# Clean up
rm -r tmp/
