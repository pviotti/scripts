#!/usr/bin/env python
# Tool to download from a Nokia mobile phone all the photos shot before a certain date
# passed as first argument.

# Author: Paolo VIOTTI <paolo.viotti@gmail.com>

# License:
# Copyright 2011 Paolo Viotti
# This script is licensed under the GNU General Public License v3.0.
# You can obtain a copy of this license here: http://www.gnu.org/licenses/gpl.html

# Usage: nokia.py [date in format %Year%Month, e.g. 201004]

import obexftp, time, sys

cli = obexftp.client(obexftp.USB)
devs = cli.discover();
try:
    dev = devs[1]
except IndexError:
    print >> sys.stderr, "Error: cannot connect to the device."
    sys.exit(-1)
if cli.connect(dev, 1) != 1:
    print >> sys.stderr, "Error: cannot connect to the device."
    sys.exit(-1)

lst = cli.list("/C:/data/Images/")
#print lst

images_db = {}
for row in lst.split("\n"):
    if row.__contains__("jpg"):
        images_db[row.split('"')[1]] = time.strptime(row.split('"')[5][:8], "%Y%m%d")

try:
    date_from = time.strptime(sys.argv[1], "%Y%m")
except ValueError:
    print >> sys.stderr, "Error: please insert a valid a date in the %Year%Month time format."
    sys.exit(-1)
except IndexError:
    print >> sys.stderr, "Usage:", sys.argv[0], " [date in format %Year%Month, e.g. 201004]"
    sys.exit(-1)

for name in images_db.keys():
    if (images_db[name] > date_from):
        print name
        data = cli.get("/C:/data/Images/" +  name)
        file = open(name, 'wb')
        file.write(data)
        file.close()

cli.disconnect()
cli.delete
