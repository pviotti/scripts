// ==UserScript==
// @name        Fr words lookup
// @namespace   my.script
// @description Lookup words on french Wiktionary upon double click
// @include     http://www.lemonde.fr/*
// @version     1
// @grant       none
// ==/UserScript==

document.ondblclick = function () {
    var sel = document.selection && document.selection.createRange().text || window.getSelection && window.getSelection().toString();
    window.open("http://fr.wiktionary.org/wiki/" + sel);
}
