#!/bin/bash

for file in `ls *.JPG`
do
  convert -normalize -quality 30 -contrast -contrast -type Grayscale   $file new/$file
done


#for file in `ls new/*.JPG`
#do
#	convert -compress Zip  $file new/pdf/`basename $file | sed 's/JPG/pdf/'`
#done

mogrify -format pdf *.JPG

pdftk *.pdf cat output appunti.pdf
