#!/bin/bash
# Simple script to change keyboard layout

current=`setxkbmap -print | awk -F"+" '/xkb_symbols/ {print $2}'`
case $current in
	'us(euro)')
		setxkbmap it
		;;
	it)
		setxkbmap us
		;;
esac

